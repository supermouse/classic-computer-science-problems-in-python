from  enum import IntEnum
from typing import List, Tuple

Nucleotide: IntEnum = IntEnum('Nucleotide', ('A', 'C', 'G', 'T'))


Codon = Tuple[Nucleotide, Nucleotide, Nucleotide]  # type alias for codons
Gene = List[Codon]  # type alias for genes

def string_of_gene(s: str) -> Gene:
    gene: Gene = []
    for i in range(0, len(s), 3):
        if (i+2) >= len(s):
            return gene
        codon = (Nucleotide[s[i]], Nucleotide[s[i+1]], Nucleotide[s[i+2]])
        gene.append(codon)
    return gene

def linear_contains(gene: Gene, key_codon: Codon) -> bool:
    for codon in gene:
        if codon == key_codon:
            return True
    return False

def binary_contains(gene: Gene, key_codon: Codon) -> bool:
    low = 0
    height = len(gene) -1
    while low <= height:
        mid = (low + height)/2
        if gene[mid] < key_codon:
            low = mid+1
        elif gene[mid] > key_codon:
            height = mid - 1
        else:
            return True
    return False