from typing import Dict
from functools import lru_cache


def f1():
    def fib(n: int) -> int:
        return fib(n-1) + fib(n-2)


def f2():
    def fib(n: int) -> int:
        if n < 2:
            return n
        return fib(n-1) + fib(n-2)


def f3():
    memo: Dict[int, int] = {
        0: 0,
        1: 1
    }

    def fib(n: int) -> int:
        if n not in memo:
            memo[n] = fib(n-1) + fib(n-2)
        return memo[n]


def f4():
    @lru_cache(maxsize=None)
    def fib(n: int) -> int:
        if n < 2:
            return n
        return fib(n-1) + fib(n-2)


def f5():
    def fib(n: int) -> int:
        if n == 0:
            return n
        last = 0
        next = 1
        for _ in range(1, n):
            last, next = next, last + next
        return next


def f6():
    def fib(n: int) -> int:
        yield 0
        if n > 0:
            yield 1
        last = 0
        next = 1
        for _ in range(1, n):
            last, next = next, last + next
            yield next
        return next
