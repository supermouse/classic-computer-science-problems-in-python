from typing import Dict, List, NamedTuple, Optional
from random import choice
from string import ascii_uppercase
from .csp import Constraint, CSP

Grid = List[List[str]]


class GridLocation(NamedTuple):
    row: int
    column: int


def generate_grid(rows: int, columns: int) -> Grid:
    return [[choice(ascii_uppercase) for c in range(columns)]
            for r in range(rows)]


def display_grid(grid: Grid):
    for row in grid:
        print(''.join(row))


def generate_domain(word: str, grid: Grid) -> List[List[GridLocation]]:
    domain: List[List[GridLocation]] = []
    height = len(grid)
    width = len(grid[0])
    length = len(word)
    for row in range(height):
        for col in range(width):
            columns = range(col, col + length)
            rows = range(row, row + length)
            if col + length <= width:
                domain.append([GridLocation(row, c) for c in columns])
                if row + length <= height:
                    domain.append(
                        [GridLocation(r, col + (r - row)) for r in rows])
            if row + length <= height:
                # top to bottom
                domain.append([GridLocation(r, col) for r in rows])
                # diagonal towards bottom left
                if col - length >= 0:
                    domain.append(
                        [GridLocation(r, col - (r - row)) for r in rows])
    return domain


class WordSearchConstraint(Constraint[str, List[GridLocation]]):
    def __init__(self, words: List[str]) -> None:
        super().__init__(words)
        self.words: List[str] = words

    def satisfied(self, assignment: Dict[str, List[GridLocation]]) -> bool:
        all_locations = [
            locs for values in assignment.values() for locs in values
        ]
        return len(set(all_locations)) == len(all_locations)


grid = generate_grid(9)
words = ["MATTHEW", "JOE", "MARY", "SARAH", "SALLY"]

locations: Dict[str, List[List[GridLocation]]] = {}
for word in words:
    locations[word] = generate_domain(word, grid)

csp = CSP(words, locations)
csp.add_constraint(WordSearchConstraint(words))

solution: Optional[Dict[str, List[GridLocation]]] = csp.backtracking_search()

for word, grid_locations in solution.items():
    # random reverse half the time
    if choice([True, False]):
        grid_locations.reverse()
    for index, letter in enumerate(word):
        (row, col) = (grid_locations[index].row, grid_locations[index].column)
        grid[row][col] = letter
display_grid(grid)