class CompressedGene:
    def __init__(self, gene: str) -> None:
        self._compress(gene)

    def _compress(self, gene: str):
        self.bit_string: int = 1
        for nuceleotide in gene.upper():
            self.bit_string <<= 2
            if nuceleotide == 'A':
                self.bit_string |= 0b00
            elif nuceleotide == 'C':
                self.bit_string |= 0b01
            elif nuceleotide == 'G':
                self.bit_string |= 0b10
            elif nuceleotide == 'T':
                self.bit_string |= 0b11
            else:
                raise ValueError(f'Invalid Nuceleotiede: {nuceleotide}')

    def _decompress(self) -> str:
        gene = ''
        for i in range(0, self.bit_string.bit_length()-1, 2):
            bits: int = self.bit_string >> i & 0b11
            if bits == 0b00:
                gene += 'A'
            elif bits == 0b01:
                gene += 'C'
            elif bits == 0b10:
                gene += 'G'
            elif bits == 0b11:
                gene += 'T'
            else:
                raise ValueError(f"Invalid bits: {bits}")
        return gene[::-1]

    def __str__(self) -> str:
        return self._decompress()
